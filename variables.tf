variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_path" {}
variable "aws_key_name" {}
variable "user_name" {}


variable "webserver_count" {
    description = "Count of WebServer"
    default = 2
}

variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "ap-south-1"
}

variable "vpc_name" {
    description = "Name of VPC"
    default = "Test VPC"
}

variable "vpc_id" {
    description = "VPC ID"
    default = "vpc-1306737a"
}

variable "amis" {
    description = "AMIs by region"
    default = {
        ap-south-1 = "ami-47205e28" # Linux
    }
}

variable "db_instance_size" {
    description = "DB Instance Size"
    default = "t2.micro"
}

variable "vpc_global_ip" {
    description = "Global IP for ssh config"
    default = "172.31.*.*"
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "172.31.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "172.31.0.0/24"
}

variable "public_subnet_id" {
	description = "Subnet ID for Public Subnet"
	default = "subnet-72affd1b"
}

variable "private_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    default = "172.31.1.0/24"
}

variable "private_subnet_id" {
	description = "Subnet ID for Private Subnet"
	default = "subnet-74affd1d"
}

variable "env" {
    description = "Env Description"
    default = "test"
}